# STAGE

De interessantste onderwerpen in de basisopleiding vond ik Immunologie, Biochemie en Web Based.

Het enige project waar ik trots op ben is die waar ik nu mee bezig ben, thema 10. Hier heb ik ook aardig wat tijd in gestoken
vergeleken met andere projecten puur omdat ik front-end interessanter en leuker vind om te doen. 
https://bitbucket.org/mjgerbens/thema10projecto-m/src/master/

Qualities:
- HTML 4/5
- CSS 4/5
- Java 3/5
- Python 3/5